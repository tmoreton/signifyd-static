import { Link } from "gatsby"
import React from "react"
import Logo from "./logo"

const Header = () => (
  <header>
    <div className="px-3 mx-auto" style={{ maxWidth: 960 }}>
      <h1 className="m-0">
        <Link to="/">
          <Logo/>
        </Link>
      </h1>
    </div>
  </header>
)

export default Header
